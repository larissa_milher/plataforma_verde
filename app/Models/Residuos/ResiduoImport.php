<?php

namespace App\Models\Residuos;

use App\Models\Residuos;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ResiduoImport implements ToModel, WithHeadingRow
{

    use Importable;

    public function model(array $row)
    {
        return new Residuo([
            'nome'=> $row['nome_comum_do_residuo'],
            'tipo'=> $row['tipo_de_residuo'],
            'categoria'=> $row['categoria'],
            'tratamento'=> $row['tecnologia_de_tratamento'],
            'classe'=> $row['classe'],
            'unidadeMedida'=> $row['unidade_de_medida'],
            'peso'=> $row['peso'],
        ]);
    }

    public function headingRow(): int
    {
        return 5;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
