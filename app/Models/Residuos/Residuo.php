<?php

namespace App\Models\Residuos;

use Illuminate\Database\Eloquent\Model;

class Residuo extends Model
{
    protected $fillable = [
        'nome', 'tipo', 'categoria', 'tratamento', 'classe', 'unidadeMedida', 'peso',
    ];

}
