<?php

namespace App\Http\Controllers\Residuos;

use App\API\ApiError;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Residuos\Residuo;
use App\Models\Residuos\ResiduoImport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ResiduoController extends Controller
{
    private $residuo;

    public function __construct(Residuo $residuo)
    {
        $this->residuo = $residuo;
    }

    /**
     * @return Residuo[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return response()->json($this->residuo->paginate(10));
    }

    /**
     *
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $residuo = $this->residuo->find($id);

        if (!$residuo) return response()->json(ApiError::errorMessage('Residuo não encontrado!', 4040), 404);

        $data = ['data' => $residuo];

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = Validator::make($request->all(), [
                'peso' => 'numeric',
            ]);

            if ($validator->fails())
                throw  new \Exception('O campo peso deve ser do tipo numérico');

            $residuoData = $request->all();
            $residuo = $this->residuo->find($id);

            if ($residuo == null)
                throw  new \Exception('Resíduo não encontrado');

            $residuo->update($residuoData);

            $return = [
                'data' => [
                    'mensagem' => 'Resíduo atualizado com sucesso!'
                ]
            ];

            return response()->json($return, 200);

        } catch (\Exception $e) {
            return response()->json(ApiError::errorMessage($e->getMessage(), 1011), 500);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $residuo = $this->residuo->find($id);

            if ($residuo == null)
                throw  new \Exception('Resíduo não encontrado');

            $residuo->delete();

            $return = [
                'data' => [
                    'mensagem' => 'Resíduo: ' . $residuo->nome . ' excluido com sucesso!'
                ]
            ];

            return response()->json($return, 200);

        } catch (\Exception $e) {
            return response()->json(ApiError::errorMessage($e->getMessage(), 1012), 500);
        }
    }

    /** * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * */
    public function importExcel(Request $request)
    {
        try {
            $nameFile = null;

            if ($request->hasFile('file') && $request->file('file')->isValid()) {

                $name = uniqid(date('HisYmd'));
                $extension = $request->file->extension();
                $nameFile = "{$name}.{$extension}";
                $upload = $request->file->storeAs('residuos', $nameFile);

                if (!$upload)
                    throw  new \Exception('Falha ao fazer importação');

                $dadosPlanilha = (new ResiduoImport)->import($upload, 'local', \Maatwebsite\Excel\Excel::XLSX);

                Storage::delete($upload);

                if (!$dadosPlanilha)
                    throw  new \Exception('Falha ao fazer importação');

                $return = [
                    'data' => [
                        'mensagem' => 'Planilha importada com sucesso!'
                    ]
                ];
            }
            else{
                $return = [
                    'data' => [
                        'mensagem' => 'Planilha não encontrada!'
                    ]
                ];
            }
            return response()->json($return, 200);

        } catch (\Exception $e) {
            return response()->json(ApiError::errorMessage($e->getMessage(), 1011), 500);
        }
    }
}
