<?php


Route::group(
    [
        'prefix'     => 'residuos',
    ],
    function() {
        Route::get('/', 'Residuos\ResiduoController@index');
        Route::get('/{id}', 'Residuos\ResiduoController@show');

        Route::put('/{id}/update', 'Residuos\ResiduoController@update');
        Route::delete('/{id}/delete', 'Residuos\ResiduoController@destroy');

        Route::post('/importarExcel', 'Residuos\ResiduoController@importExcel');
    }
);
